import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { CourseComponent } from './course.component';
import { MainComponent } from './main.component';
import { HttpService } from './http.service';

const routes = [
  {
    path: '',                                                                   // при localhost:<>/ будет отображен AC
    component: MainComponent,
  },
  {
    path: 'courses',
    component: AppComponent,
  },
  {
    path: 'courses/course/:id',
    component: CourseComponent,
  },
];

@NgModule({
  declarations: [
    AppComponent,
    CourseComponent,
    MainComponent
  ],
  imports: [
    BrowserModule, HttpClientModule, RouterModule.forRoot(routes)
  ],
  providers: [
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    },
    HttpService
  ],
  bootstrap: [MainComponent]
})
export class AppModule { }
