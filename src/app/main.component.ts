import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpService } from './http.service';

@Component({
  selector : 'app-root',
  templateUrl: './main.component.html',
  providers: [
    HttpService
  ]
})
export class MainComponent implements OnInit {
   constructor(private router: Router, private activatedRoute: ActivatedRoute) {}
   
   ngOnInit() {
      this.router.navigateByUrl('courses');
   }
}
