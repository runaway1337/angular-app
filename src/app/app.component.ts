import { Component, OnInit } from '@angular/core';
import { HttpService } from './http.service';

import { cl, MAX_PAGES } from './constants';

@Component({
  selector: 'app-comp',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [
    HttpService,
  ]
})
export class AppComponent implements OnInit {
  private data;
  private currentPage: number;
  private loading: boolean;
  public currentUrl: string;
  

  constructor(private httpService: HttpService, ) {
    this.data = [];
    this.currentUrl = '';
    this.currentPage = 1; 
  }

  private  loadData = () => {
    if (this.currentPage <= MAX_PAGES) {  // альтернативно можно взять метод поиска максимальной страницы
      let currentUrl = `https://postnauka.ru/api/v1/posts?page=${this.currentPage}&term=courses`;
      this.httpService.getReq3(currentUrl).then(
        () => {
          cl(`Page ${this.currentPage} of ${MAX_PAGES} loaded!`);
          ++this.currentPage;
          this.loadData();
        },
        err => {
          cl(err);
        }
      );
    } else {
      cl('Success!');
      this.data = this.httpService.getFullData();
      this.loading = false;
    }
  }

  ngOnInit() {
    this.loading = true;
    this.loadData();
  }

}
