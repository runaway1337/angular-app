import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable()
export class HttpService {
  private currentUrl: string;
  public data: any;
  private currentPage: number;

  constructor(private http: HttpClient) {
    this.data = [];
  }
  
  // public getDataById = (id) => {
  //   return this.data[0].find((iter) => {
  //     return (iter.id === id);
  //   });
  // }

  public getFullData = () => {
    return this.data;
  }

  public getReq3 = (url: string) => {
    return new Promise ((resolve,reject) => {
      this.http.get(url, {responseType: 'text'}).toPromise().then(
        ok => {
          if (this.data.length > 0) {             // т.к. data - массив json'ов, чтобы добавить данные в конец
            let newData = JSON.parse(ok);         // массива, делаем это
            newData.forEach(element => {
              if(element.category !== undefined
                && element.category.link === 'courses') 
                this.data[0].push(element);          
            });
          }
          else this.data.push(JSON.parse(ok));
          resolve (ok);
        },
        err => {
          reject (new Error('error: '));
        }
      );
    });
  }
}
