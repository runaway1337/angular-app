import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';

import { HttpService } from './http.service';


@Component({
  templateUrl: './course.component.html',
  providers: [
    HttpService
  ]
})
export class CourseComponent implements OnInit{
  private routeSubscription: any;
  private courseId: number;
  private course: string;

  constructor(private router: Router, private activatedRoute: ActivatedRoute, 
              private httpService: HttpService, private http: HttpClient)
  {
    this.routeSubscription = activatedRoute.params.subscribe(params =>                       
      {
          this.courseId = params['id'];                                                   
      }
    );
    this.course = '';
  }

  ngOnInit() {
    this.http.get(
      `https://postnauka.ru/api/v2/track/data?id=wp:${this.courseId}&expand=image,link,stat,merchandise_id`
      , {responseType: 'text'}).toPromise()
      .then(
        ok => {
          this.course = JSON.parse(ok);
        }
      );
  }
}
